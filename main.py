from datetime import datetime
import webbrowser
from dataclasses import dataclass
from enum import Enum
from tkinter import Listbox, BOTH, PanedWindow, Entry, Button, Text, Menu, Tk, W, END, Scrollbar, RIGHT, Y, StringVar, \
    NORMAL, DISABLED
from tkinter.ttk import Frame
from tkinter.ttk import Label
from typing import Optional


@dataclass
class DT:
    ms: int
    seconds: int
    minutes: int
    hours: int


class State(Enum):
    IDLE = "IDLE"
    STARTED = "STARTED"
    STOPPED = "STOPPED"


@dataclass
class Task:
    number: str
    title: str
    description: str
    url: str
    started: Optional[str]
    stopped: Optional[str]
    milliseconds: int


class Client:

    def __init__(self):
        self.tasks = []

    def fetch_tasks(self):
        for i in range(15):
            task = Task(
                number=f"abc{i}",
                title=f"Task-{i}",
                description=f"Description: task {i}",
                url="",
                milliseconds=0,
                started=None,
                stopped=None
            )
            self.tasks.append(task)
        return self.tasks


class NewTaskWindow:

    def __init__(self, parent):
        self.toplevel = Tk()
        self.toplevel.protocol("WM_DELETE_WINDOW", self.destroy)
        self.toplevel.resizable(False, False)

        self.toplevel.title("Create New Task")

        self.toplevel.columnconfigure(0, weight=1)
        self.toplevel.columnconfigure(1, weight=1)
        self.toplevel.columnconfigure(2, weight=1)

        padding = {"padx": 5, "pady": 5, "sticky": W}  # noqa

        self.number = None
        self.title = None
        self.description = None
        self.url = None

        self.label_number = Label(self.toplevel, text="Number")
        self.label_number.grid(row=0, column=0, columnspan=1, **padding)
        self.number_edit = Entry(self.toplevel)
        self.number_edit.grid(row=0, column=1, columnspan=2, **padding)
        self.label_title = Label(self.toplevel, text="Title")
        self.label_title.grid(row=1, column=0, columnspan=1, **padding)
        self.title_edit = Entry(self.toplevel)
        self.title_edit.grid(row=1, column=1, columnspan=2, **padding)
        self.description_label = Label(self.toplevel, text="Description")
        self.description_label.grid(row=2, column=0, columnspan=3, **padding)
        self.description_edit = Text(self.toplevel, width=30, height=3)
        self.description_edit.grid(row=3, column=0, columnspan=3, **padding)
        self.save_button = Button(self.toplevel, text="SAVE")
        self.save_button.bind("<Button-1>", self.on_save_button_click)
        self.save_button.grid(row=4, column=0, columnspan=3, **padding)

        self.toplevel.mainloop()

    def on_save_button_click(self, event):
        self.number = self.number_edit.get()
        self.title_edit = self.title_edit.get()
        self.description = self.description_edit.get('1.0', END)
        self.url = ""
        self.destroy()

    def destroy(self):
        self.toplevel.quit()
        self.toplevel.destroy()


class Application(Frame):

    def __init__(self, root_component):
        super().__init__()
        self.root = root_component
        self.client = Client()

        self.current_task = None
        self.tasks = []

        self.fetch_tasks()

        self.init_ui()

    def init_ui(self):
        self.master.title("Local Task tracker")  # noqa
        self.master.geometry("300x500")  # noqa
        self.master.resizable(False, False)  # noqa

        self.columnconfigure(0, weight=1)

        self.main_menu = Menu(self, tearoff=0)  # noqa
        self.master.config(menu=self.main_menu)  # noqa
        self.tasks_menu = Menu(self.main_menu, tearoff=0)  # noqa
        self.tasks_menu.add_command(label="Create New Task", command=self.on_menu_tasks_create_new_task_click)
        self.tasks_menu.add_command(label="Create Report", command=self.on_menu_tasks_report_click)
        self.main_menu.add_cascade(label="Tasks", menu=self.tasks_menu)

        self.title_label = Label(self, text="Current Task")  # noqa
        self.title_label.grid(row=0, column=0, sticky="nsew")

        self.timer_panel = PanedWindow(self, orient="vertical", borderwidth=2)  # noqa
        self.timer_panel.grid(row=1, column=0, sticky="nsew")

        self.timer_string_var = StringVar()  # noqa
        self.timer = Label(self.timer_panel, text="00:00:00", font=("Arial", 24, ""), borderwidth=2, relief="groove", textvariable=self.timer_string_var)  # noqa
        self.timer_panel.add(self.timer)

        self.timer_buttons_panel = PanedWindow(self.timer_panel, orient="horizontal")  # noqa
        self.timer_panel.add(self.timer_buttons_panel)

        self.start_button = Button(self.timer_panel, text="START")  # noqa
        self.start_button.bind("<Button-1>", self.on_start_button_click)
        self.timer_buttons_panel.add(self.start_button)
        self.stop_button = Button(self.timer_panel, text="STOP")  # noqa
        self.stop_button.bind("<Button-1>", self.on_stop_button_click)
        self.timer_buttons_panel.add(self.stop_button)

        self.task_description_panel = PanedWindow(self, orient="vertical")  # noqa
        self.timer_panel.add(self.task_description_panel)

        self.task_number_label = Label(self.task_description_panel, text="Number")  # noqa
        self.task_description_panel.add(self.task_number_label)
        self.task_number = Entry(self.task_description_panel)  # noqa
        self.task_description_panel.add(self.task_number)
        self.task_title_label = Label(self.task_description_panel, text="Title")  # noqa
        self.task_description_panel.add(self.task_title_label)
        self.task_title = Entry(self.task_description_panel)  # noqa
        self.task_description_panel.add(self.task_title)
        self.task_description_label = Label(self.task_description_panel, text="Description")  # noqa
        self.task_description_panel.add(self.task_description_label)
        self.task_description = Text(self.task_description_panel, height=3)  # noqa
        self.task_description_panel.add(self.task_description)
        # self.task_url_label = Label(self.task_description_panel, text="Task URL", cursor="hand2", foreground="blue")
        # url = self.current_task.url if self.current_task else "https://ya.ru"
        # self.task_url_label.bind("<Button-1>", lambda e: self.open_url(url))
        # self.task_description_panel.add(self.task_url_label)

        self.task_list_label = Label(self, text="Task List")  # noqa
        self.task_list_label.grid(row=2, column=0, sticky="nsew")

        self.task_list_panel = PanedWindow(self, orient="vertical", borderwidth=2)  # noqa
        self.task_list_panel.grid(row=3, column=0, sticky="nsew")

        self.task_list_scrollbar = Scrollbar(self.root)  # noqa
        self.task_list_scrollbar.pack(side=RIGHT, fill=Y)

        self.task_list = Listbox(self.task_list_panel, height=10)  # noqa
        self.task_list.config(yscrollcommand=self.task_list_scrollbar.set)
        self.task_list_scrollbar.config(command=self.task_list.yview)
        self.task_list.bind('<<ListboxSelect>>', self.on_task_list_select)
        self.task_list.bind("<MouseWheel>", lambda event: self.scroll_tasks(event, self.task_list))
        self.task_list_panel.add(self.task_list)

        self.timer = Label(self.master)  # noqa
        self.timer_started = False  # noqa

        self.fetch_tasks()
        self.fill_task_list()

        self.disable_start_button()
        self.disable_stop_button()

        self.pack(fill=BOTH, expand=True)

    def enable_start_button(self):
        self.start_button.config(state=NORMAL)

    def disable_start_button(self):
        self.start_button.config(state=DISABLED)

    def enable_stop_button(self):
        self.stop_button.config(state=NORMAL)

    def disable_stop_button(self):
        self.stop_button.config(state=DISABLED)

    @staticmethod
    def milliseconds_to_time(ms: int) -> DT:
        ms = ms // 1000
        time = ms % (24 * 3600)
        hours = time // 3600
        time %= 3600
        minutes = time // 60
        time %= 60
        seconds = time
        return DT(ms=ms, seconds=seconds, minutes=minutes, hours=hours)

    def start_timer(self):
        self.timer_started = True  # noqa
        self.timer.after(1000, self.on_timer_tick)

    def stop_timer(self):
        self.timer_started = False  # noqa
        self.timer.after_cancel('timer')

    def on_timer_tick(self):
        print(self.current_task)
        if self.current_task:
            self.current_task.milliseconds += 1000
            self.recalculate_time_and_set_text(self.current_task.milliseconds)
        if self.timer_started:
            self.start_timer()

    def recalculate_time_and_set_text(self, ms):
        dt = self.milliseconds_to_time(ms)
        hours = dt.hours
        if hours < 10:
            hours = f"0{hours}"
        minutes = dt.minutes
        if minutes < 10:
            minutes = f"0{minutes}"
        seconds = dt.seconds
        if seconds < 10:
            seconds = f"0{seconds}"
        self.timer_string_var.set(f"{hours}:{minutes}:{seconds}")

    @staticmethod
    def scroll_tasks(event, sc):
        sc.yview_scroll(int(-4*(event.delta/120)), "units")

    def fill_task_list(self):
        for task in self.tasks:
            self.task_list.insert(END, task.title)

    def fetch_tasks(self):
        self.tasks = self.client.fetch_tasks()

    def on_task_list_select(self, event):
        selection = self.task_list.curselection()
        if selection:
            selection = selection[0]
            self.current_task = self.tasks[selection]
            self.reload_task()
            self.enable_start_button()
            self.disable_start_button()

    def reload_task(self):
        self.task_number.delete(0, END)
        self.task_number.insert(0, self.current_task.number)
        self.task_title.delete(0, END)
        self.task_title.insert(0, self.current_task.title)
        self.task_description.delete('1.0', END)
        self.stop_timer()
        self.enable_start_button()
        self.disable_stop_button()
        self.task_description.insert(END, self.current_task.description)
        self.recalculate_time_and_set_text(self.current_task.milliseconds)

    def on_start_button_click(self, event):
        print('start')
        if not self.current_task:
            print('no current task')
            return
        self.start_timer()
        self.disable_start_button()
        self.enable_start_button()
        if self.current_task and self.current_task.started is None:
            now = datetime.now()
            self.current_task.started = now.strftime("%Y_%m_%d_%I_%M_%S_%p")
            self.current_task.stopped = None

    def on_stop_button_click(self, event):
        print('stop')
        self.stop_timer()
        if self.current_task:
            self.disable_stop_button()
            self.enable_start_button()
            now = datetime.now()
            self.current_task.stopped = now.strftime("%Y_%m_%d_%I_%M_%S_%p")

    @staticmethod
    def open_url(url: str):
        webbrowser.open_new(url)

    def on_menu_tasks_report_click(self):
        pass

    def on_menu_tasks_create_new_task_click(self):
        view = NewTaskWindow(self)
        task = Task(
            number=view.number,
            title=view.title,
            description=view.description,
            url=view.url,
            milliseconds=0,
            started=None,
            stopped=None
        )
        print(task)


if __name__ == "__main__":
    from ttkthemes import ThemedTk

    root = ThemedTk(theme="plastic")  # noqa
    app = Application(root)
    root.mainloop()
